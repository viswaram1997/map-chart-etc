import React, { Component } from 'react'
import { ClipLoader } from 'react-spinners';
export default class Name extends Component {
    constructor(props) {
        super(props);
        this.state = {
          loading: true
        }
      }
    componentWillReceiveProps(props){
        // console.log('update',props)
    }
    componentWillUpdate(){
        console.log('will update')
    }
    render() {
        return (
            <div>
                    <h1>{this.props.name}</h1>
                    <div className='sweet-loading'>
        <ClipLoader
          color={'black'} 
          loading={this.state.loading} 
        />
      </div>
            </div>
        )
    }
}
