// import React from 'react';
// import DatePicker from 'react-datepicker';
import moment from 'moment';
 
// import 'react-datepicker/dist/react-datepicker.css';
 
// // CSS Modules, react-datepicker-cssmodules.css
// // import 'react-datepicker/dist/react-datepicker-cssmodules.css';
 
// export  default class Date extends React.Component {
//   constructor (props) {
//     super(props)
//     this.state = {
//       startDate: moment()
//     };
//     this.handleChange = this.handleChange.bind(this);
//   }
 
//   handleChange(date) {
//     this.setState({
//       startDate: date
//     });
//     console.log(date.toDate())
//   }
 
//   render() {
//     return <DatePicker
//         selected={this.state.startDate}
//         onChange={this.handleChange}
        
//     />
//   }
// }

import React from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

export default class Example extends React.Component {
  constructor(props) {
    super(props);
    this.handleDayChange = this.handleDayChange.bind(this);
    this.state = {
      selectedDay: undefined,
      isDisabled: false,
    };
  }
  handleDayChange(selectedDay, modifiers) {
    this.setState({
      selectedDay,     
    });
    var monthNum = moment(selectedDay).format('D');  // assuming Jan = 1
    console.log(monthNum)
  
  }
  render() {
    const range = {
  from: new Date(2018, 2, 14),
  to: new Date(2018, 2, 18)
}
    const { selectedDay } = this.state;
    return (
      <div>
      
        <DayPickerInput
          value={selectedDay}
          onDayChange={this.handleDayChange}
          dayPickerProps={{
            selectedDays: selectedDay,
            disabledDays: {
              range
            },
          }}
        />
      </div>
    );
  }
}