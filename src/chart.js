import React, { Component } from 'react'
import ResponsiveBar from '../node_modules/nivo/lib/components/charts/bar/ResponsiveBarCanvas'

export default class Chart extends Component {
    render() {

        return (
            <div className="App">
                <ResponsiveBar
                    data={[
                        {
                            "country": "AD",
                            "hot dog": 96,
                            "hot dogColor": "hsl(225, 70%, 50%)",
                            "burger": 115,
                            "burgerColor": "hsl(20, 70%, 50%)",
                            "sandwich": 55,
                            "sandwichColor": "hsl(357, 70%, 50%)",



                        },
                        {
                            "country": "AE",
                            "hot dog": 129,
                            "hot dogColor": "hsl(36, 70%, 50%)",
                            "burger": 170,
                            "burgerColor": "hsl(200, 70%, 50%)",
                            "sandwich": 71,
                            "sandwichColor": "hsl(37, 70%, 50%)",

                        },
                        {
                            "country": "AF",
                            "hot dog": 151,
                            "hot dogColor": "hsl(120, 70%, 50%)",
                            "burger": 125,
                            "burgerColor": "hsl(122, 70%, 50%)",
                            "sandwich": 48,
                            "sandwichColor": "hsl(132, 70%, 50%)",

                        }


                    ]}
                    keys={[
                        "hot dog",
                        "burger",
                        "sandwich",

                    ]}
                    indexBy="country"
                    margin={{
                        "top": 50,
                        "right": 130,
                        "bottom": 50,
                        "left": 60
                    }}
                    padding={0.3}
                    groupMode="grouped"
                    colors={['#4AC1F9', '#008BDB', '#FF8214']}
                    colorBy="id"
                    borderColor="inherit:darker(1.6)"
                    axisBottom={false}
                    axisLeft={false}
                    enableLabel={false}
                    labelSkipWidth={12}
                    labelSkipHeight={12}
                    labelTextColor="inherit:darker(1.6)"
                    animate={true}
                    enableGridY={false}
                    motionStiffness={90}
                    motionDamping={15}
                    legends={[
                        {
                            "dataFrom": "keys",
                            "anchor": "bottom-right",
                            "direction": "column",
                            "translateX": 120,
                            "itemWidth": 100,
                            "itemHeight": 20,
                            "itemsSpacing": 2,
                            "symbolSize": 20
                        }
                    ]}
                />

            </div>
        )
    }
}
