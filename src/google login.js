import React, { Component } from 'react'
import GoogleLogin from 'react-google-login';   
// import GoogleAuthorize from 'react-google-authorize';
export default class Google extends Component {
    responseGoogle(response) {
        console.log(JSON.stringify(response));
      }

    render() {
        return (
            <div>
           <GoogleLogin
    clientId="971556861778-jac45puk83vqi78pteoe2qv61g7pr56b.apps.googleusercontent.com"
    buttonText="Login with google"
    onSuccess={this.responseGoogle}
    onFailure={this.responseGoogle}
  />
            </div>
        )
    }
}
