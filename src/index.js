import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import FunctionalComponent from "./functionalComponent"
import Router from "./router"
import  Chart from "./chart"
import Date from "./date picker"
import Google from "./google login"
import Tags from "./tags"
import Cal from "./cal"
ReactDOM.render(<Chart />, document.getElementById('root'));
registerServiceWorker();
